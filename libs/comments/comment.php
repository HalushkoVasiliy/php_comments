<?php
/**
 * Created by PhpStorm.
 * User: GVD
 * Date: 29.11.2017
 * Time: 13:40
 */
    // Проверка на наличие объекта класса User и создание если таковой отсутствует
    if ( @!$user ){
        require_once('../Sys/Classes.php');
        $new_message = new Comment('comments');
    }

// if(isset($_POST['goAuth'])){ //Проверка пришли ли данные методом пост

        $message = strip_tags(addslashes($_POST['message'])); // + удаление тегов и скриптов + получение сообщения
        $parent_id = $_POST['comment']; // получаем айди комментария
        $to_who = $_POST['where']; // получаем чье сообщение откомментировали
        $login = $_POST['name']; // кто автор нового комментария

        // Создание новых объектов классов Комментарий и Юзер
        $new_comment = new Comment( 'comments');
        $tree = new Comment( 'comments');
        $build = new Comment( 'comments');
        $email = new User('users');

        // Проверка пришел ли новый комментарий с содержимым
        if ($message != null) {
            $result = $new_message->addComment($login, $message, $parent_id); // вызов функции класса Коммент на добавление нового сообщения
            $new_comment = $new_comment -> get_newComment($result); // Получение только что добавленного комментария
            $parent_id1 = $new_comment[0]['parent_id']; // Получем парент айди комментария

            // Строим новый комментарий по шаблону
            $trees = $tree->form_tree($new_comment);
                echo $build->build_tree($trees, $parent_id1);

            // Процедура отправки оповещений на эмейлы администратора и того, чей коммент откомментировали
            require_once 'Send.php';
        }
// }
