<?php
    /**
     * Created by PhpStorm.
     * User: GVD
     * Date: 27.11.2017
     * Time: 12:50
     * Проверка формы авторизации
     */

    //Проверка на наличие объекта класса User и создание если таковой отсутствует
    if ( @!$user ){
            require_once('../Sys/Classes.php');
            $user = new User( 'users');
    }

    //Присвоение необходимым для регистрации нового пользователя переменным соответствующих значений метода POST
    $login = strip_tags($_POST['user_name']); // + удаление тегов и скриптов
    $password = md5($_POST['password']); //Хэширование пароля методом мд5
    $users = $user -> getUserWhereLogin("$login");

    //Проверка на наличие в сессии пользователя, вывод сообщений
    if($users !=0 ){
        if($password == $users[0]['pass'])
        {
            $_SESSION['authUser'] = 1;
            $_SESSION['userLogin'] = $login;
            require_once 'forms.php';
        }
        else {
            echo '<div class="message red" id="message">Вы ввели не правильный пароль</div>';
        }
    }
    else {
        echo '<div class="message red" id="message">Такого пользователя не существует</div>';
    }

