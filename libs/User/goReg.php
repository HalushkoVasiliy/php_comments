<?php
    /**
     * Created by PhpStorm.
     * User: GVD
     * Date: 27.11.2017
     * Time: 12:50
     * Проверка формы регистрации пользователя
     */


    //Присвоение необходимым для регистрации нового пользователя переменным соответствующих значений метода POST
    $login = strip_tags($_POST['new_user_name']); // + удаление тегов и скриптов
    $password = strip_tags($_POST['password']); // + удаление тегов и скриптов
    $r_password = strip_tags($_POST['r_password']); // + удаление тегов и скриптов
    $email = strip_tags($_POST['email']); // + удаление тегов и скриптов

    //Проверка на наличие объекта класса User и создание если таковой отсутствует
    if ( @!$user ){
        require_once('../Sys/Classes.php');
        $user = new User("users");
    }

    //Валидация формы регистрации на стороне PHP и вызов функции класса User
    if(preg_match('/^[a-z0-9]{6,10}$/', $login)) {
        if(preg_match('/^[a-z0-9]{6,10}$/', $password)){
            if($password == $r_password){
                $user->addUser($login, $email, md5($password));
                echo '<div class="message green" id="message">Регистрация прошла успешно</div>';
            }
            else
                echo '<div class="message red" id="message">Не правильно повторен пароль</div>';
        }
    }
    else {
        echo '<div class="message red" id="message">Пароль и логин должны состоять из латинских букв и цифр 6-10шт</div>';
    }
