<?php
/**
 * Created by PhpStorm.
 * User: GVD
 * Date: 21.11.2017
 * Time: 17:32
 */

//Дополнительный старт сессии
@session_start();

//Подключение файла с подключением к БД
require_once("bd_connection.php");

//Класс регистрации, авторизации пользователей
class User extends bg_connection {

    //Получение таблицы БД для класса
    private $table;

    //Функция конструктор
    function __construct($table)
    {
        parent::__construct();
        $this->table = $table;
    }

    //Функция проверки авторизации пользователя
    /**
     * @param $login
     * @return array|int
     */
    function getUserWhereLogin($login){
        $user = $this->selectPDO("*", $this->table, "login='$login'");
        if(count($user) > 0) return $user;
        else return 0;
    }

    /**
     * @param $avtor_id1
     * @return array|int
     */
    function getEmailWhereLogin($avtor_id1){
        $email = $this->selectPDO("*", $this->table, "login='$avtor_id1'");
        if(count($email) > 0) return $email;
        else return 0;
    }
    //Функция добавления нового пользователя в БД с логином, паролем и емаил адресом
    /**
     * @param $login
     * @param $email
     * @param $password
     */
    function addUser($login, $email, $password){
        $this -> insertPDO($this->table, "`login`,`email`, `pass`", "'$login','$email', '$password'");
    }
}

// Класс комментариев
class Comment extends bg_connection {

    //Получение таблицы БД для класса
    private $table;

    //Функция конструктор
    function __construct($table)
    {
        parent::__construct();
        $this->table = $table;

    }

    //Функция получения всех комментариев в массив
    /**
     * @return array|int
     */
    function getComments()
    {
        $comment = $this->selectPDO("*", $this->table, "post_id='1'");
        if (count($comment) > 0) return $comment;
        else return 0;
    }
    function get_newComment($result)
    {
        $new_comment = $this->selectPDO("*", $this->table, "id='$result'");
        if (count($new_comment) > 0) return $new_comment;
        else return 0;
    }
    //Перебор многомерного массива комментариев, получение древовидной формы
    /**
     * @param $comments
     * @return array|bool
     */
    function form_tree($comments)
    {
        if (!is_array($comments)) {
            return false;
        }
        $tree = array();
        foreach ($comments as $value) {
            $tree[$value['parent_id']][] = $value;
        }
        return $tree;
    }

    //Функция построения дерева комментариев
    /**
     * @param $cats
     * @param $parent_id
     * @return bool|string
     */
    function build_tree($cats, $parent_id)
    {
        if (is_array($cats) && isset($cats[$parent_id])) {
            $tree = '<ul class="ultra ul-treefree ul-dropfree">';
            foreach ($cats[$parent_id] as $cat) {
                $tree .= '<li class="item" data-atr="'. $cat['id'] .'">' . '<div class="block1">' .$cat['time_date']. '</div>'.'<div class="block2">' .$cat['avtor']. '</div>'.'<div class="block3">' .$cat['message']. '</div>';
                    if(@$_SESSION['authUser'] !=0){
                        $tree .= '<div class="answer" id="parent_id" data-id="'.$cat['id'].'" data-to-who="'.$cat['avtor'].'"></div>';
                        if($_SESSION['userLogin'] == 'vasiliy'){
                            $tree .= '<div class="delete" id="delete_comm" data-id-com="'.$cat['id'].'"></div>';
                        }
                    }
                    else{
                        $tree .= '<div class="ahtung">Чтобы добавить ваш комментарий вы должны быть авторизованны</div>';
                    }
                        $tree .= $this->build_tree($cats, $cat['id']);
                $tree .= '</li>';
            }
            $tree .= '</ul>';
        } else {
            return false;
        }
        return $tree;
    }

    //Функция добавления нового комментария в базу данных

    /**
     * @param $login
     * @param $message
     * @param $parent_id
     * @return bool
     */
    function addComment($login, $message, $parent_id){
       return $this -> insertPDO($this->table, "`avtor`,`message`, `parent_id`", "'$login','$message', '$parent_id'");
    }

    function deleteComment($delete_comm_id){
        return $this -> deletePDO($this->table, "`id`=$delete_comm_id");
    }
}

//Класс отправки оповещений на почту
class SendMailSmtpClass {

    /**
     *
     * @var string $smtp_username - логин
     * @var string $smtp_password - пароль
     * @var string $smtp_host - хост
     * @var string $smtp_from - от кого
     * @var integer $smtp_port - порт
     * @var string $smtp_charset - кодировка
     *
     */
    public $smtp_username;
    public $smtp_password;
    public $smtp_host;
    public $smtp_from;
    public $smtp_port;
    public $smtp_charset;

    public function __construct($smtp_username, $smtp_password, $smtp_host, $smtp_from, $smtp_port = 25, $smtp_charset = "utf-8") {
        $this->smtp_username = $smtp_username;
        $this->smtp_password = $smtp_password;
        $this->smtp_host = $smtp_host;
        $this->smtp_from = $smtp_from;
        $this->smtp_port = $smtp_port;
        $this->smtp_charset = $smtp_charset;
    }

    /**
     * Отправка письма
     *
     * @param string $mailTo - получатель письма
     * @param string $subject - тема письма
     * @param string $message - тело письма
     * @param string $headers - заголовки письма
     *
     * @return bool|string В случаи отправки вернет true, иначе текст ошибки    *
     */
    function send($mailTo, $subject, $message, $headers) {
        $contentMail = "Date: " . date("D, d M Y H:i:s") . " UT\r\n";
        $contentMail .= 'Subject: =?' . $this->smtp_charset . '?B?'  . base64_encode($subject) . "=?=\r\n";
        $contentMail .= $headers . "\r\n";
        $contentMail .= $message . "\r\n";

        try {
            if(!$socket = @fsockopen($this->smtp_host, $this->smtp_port, $errorNumber, $errorDescription, 30)){
                throw new Exception($errorNumber.".".$errorDescription);
            }
            if (!$this->_parseServer($socket, "220")){
                throw new Exception('Connection error');
            }

            $server_name = $_SERVER["SERVER_NAME"];
            fputs($socket, "HELO $server_name\r\n");
            if (!$this->_parseServer($socket, "250")) {
                fclose($socket);
                throw new Exception('Error of command sending: HELO');
            }

            fputs($socket, "AUTH LOGIN\r\n");
            if (!$this->_parseServer($socket, "334")) {
                fclose($socket);
                throw new Exception('Autorization error');
            }



            fputs($socket, base64_encode($this->smtp_username) . "\r\n");
            if (!$this->_parseServer($socket, "334")) {
                fclose($socket);
                throw new Exception('Autorization error');
            }

            fputs($socket, base64_encode($this->smtp_password) . "\r\n");
            if (!$this->_parseServer($socket, "235")) {
                fclose($socket);
                throw new Exception('Autorization error');
            }

            fputs($socket, "MAIL FROM: <".$this->smtp_username.">\r\n");
            if (!$this->_parseServer($socket, "250")) {
                fclose($socket);
                throw new Exception('Error of command sending: MAIL FROM');
            }

            $mailTo = ltrim($mailTo, '<');
            $mailTo = rtrim($mailTo, '>');
            fputs($socket, "RCPT TO: <" . $mailTo . ">\r\n");
            if (!$this->_parseServer($socket, "250")) {
                fclose($socket);
                throw new Exception('Error of command sending: RCPT TO');
            }

            fputs($socket, "DATA\r\n");
            if (!$this->_parseServer($socket, "354")) {
                fclose($socket);
                throw new Exception('Error of command sending: DATA');
            }

            fputs($socket, $contentMail."\r\n.\r\n");
            if (!$this->_parseServer($socket, "250")) {
                fclose($socket);
                throw new Exception("E-mail didn't sent");
            }

            fputs($socket, "QUIT\r\n");
            fclose($socket);
        } catch (Exception $e) {
            return  $e->getMessage();
        }
        return true;
    }

    private function _parseServer($socket, $response) {
        while (@substr($responseServer, 3, 1) != ' ') {
            if (!($responseServer = fgets($socket, 256))) {
                return false;
            }
        }
        if (!(substr($responseServer, 0, 3) == $response)) {
            return false;
        }
        return true;

    }
}