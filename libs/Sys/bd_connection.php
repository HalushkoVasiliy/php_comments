<?php
/**
 * Created by PhpStorm.
 * User: GVD
 * Date: 21.11.2017
 * Time: 15:56
 */

class bg_connection
{

    private $PDO;
    public $lastId;
    /**
     * bg_connection constructor.
     * @param string $dbname
     * @param string $host
     * @param string $user
     * @param string $password
     */
    function __construct($dbname = "gvd_php", $host = "localhost", $user = "gvd", $password = "qaz123wsx")
    {
        try {
            $this->PDO = new PDO("mysql:host={$host};dbname={$dbname}", $user, $password);
            return $this->PDO;
        } catch (PDOException $e) {
            echo "" . $e->getMessage();
        }
    }

    function selectPDO($cols, $dbname, $where = "")
    {
        $sql = "SELECT {$cols} FROM {$dbname} WHERE {$where}";
        $rs = $this->PDO->query($sql);
        if ($where != "") {
            $row = $rs->fetchAll((PDO::FETCH_ASSOC));
            return $row;
        } else {
            $row = $rs->fetchAll(PDO::FETCH_ASSOC);
            return $row;
        }
    }
    /**
     * @param $db
     * @param $cols
     * @param string $values
     * @return bool
     */
    function insertPDO($db, $cols, $values = "")
    {
        if ($cols == "") echo "бла бла бла <br/>";
        $sql = "INSERT INTO {$db} ({$cols}) VALUES ({$values})";
        if ( $this->PDO->query($sql) ) {
            $lastId = $this->PDO->lastInsertId();
            return $lastId;
        }
        else {
            return false;
        }
    }

    /**
     * @param $db
     * @param $what
     * @param $val
     * @param $where
     */
    function updatePDO($db, $what, $val, $where)
    {
        $sql = "UPDATE {$db} SET {$what}='{$val}' {$where}";
        $this->PDO->query($sql);
    }

    /**
     * @param $db
     * @param $where
     * @return PDOStatement
     */
    function deletePDO($db, $where)
    {
        $sql = "DELETE FROM {$db} WHERE {$where}";
        $this->PDO->prepare($sql);
        $result_delete = $this->PDO->query($sql);
//        var_dump($result_delete);
//        var_dump($where);
    }
}
//DELETE FROM `comments` WHERE 1