<?php error_reporting( E_ALL );

/**
 * Created by PhpStorm.
 * User: GVD
 * Date: 16.11.2017
 * Time: 13:58
 */

    //Старт сессии
    @session_start();
    //Подключение классов
    require_once("libs/Sys/Classes.php");
        //Подключение к таблицам
        $user = new User("users");
        $comment = new Comment( 'comments');
        $tree = new Comment( 'comments');
        $build = new Comment( 'comments');
        $new_message = new Comment('comments');

    //Функция выхода
    if(isset($_GET['logout'])){
        session_unset($_SESSION['authUser']);
        session_unset($_SESSION['userLogin']);
        header("Location: /");
    }
?>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Page comments</title>
    <!-- Подключение стилей -->
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/normalize.css">
</head>
<body>

    <!-- Контент -->
    <!-- Панель меню -->
    <div class="menu">
    </div>
        <?php //формы авторизации и регистрации
            require_once 'libs/User/forms.php';
        ?>

    <!-- Блок пост -->
    <div class="post">
        <img src="img/12667YinokqvgJmxOPBSCjDdNhyZ.jpg" alt="/">
        <?php //Комментирование главного поста 1 уровень
            if(@$_SESSION['authUser'] !=0) {
                $cat['id'] = 0;
                echo '<div class="answer" id="parent_id" data-id="' . $cat['id'] . '"></div>';
            }
        ?>
    </div>

    <!-- Вывод результатов отправок форм -->
    <div class="result"></div>

    <!-- Вызов функций построения дерева комментариев -->
    <div class="comments">
        <?php
            $comments = $comment -> getComments();
            $trees = $tree->form_tree($comments);
                echo $build->build_tree($trees, 0);
        ?>
    </div>

    <!-- Форма отправки комментариев -->
    <form method="post" id="new_comment">
        <p><input type="text"  name="message" placeholder="Введите комментарий" required/></p>
        <p><input name="name" type="hidden" value="<?php echo $_SESSION['userLogin'] ?>"/></p>
        <p><input id="submit" name="SendComment" type="submit" value="Отправить"/></p>
        <img class="close_new_comment" src="img/close.png">
    </form>
    <form method="post" id="delete_comment">
        <p><input id="submit" name="SendComment" type="submit" value="Удалить"/></p>
    </form>
<!--    <form method="post" id="search_all_your_comments">-->
<!--        <p><input id="submit" name="SendComment" type="submit" value="Удалить"/></p>-->
<!--    </form>-->

    <!-- Подключение скриптов   -->
    <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <script type="text/javascript" src="js/ajax.js"></script>
</body>
</html>