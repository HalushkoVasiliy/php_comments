//Аякс отправка формы регистрации на сервер
$('#reg_form').on('submit',function(e) {
    e.preventDefault();
    var theForm = $(this); //Отправка формы
    $.post("libs/User/goReg.php", theForm.serialize(), //принимающий файл
        function( data ) {
            $(".result").html(data);
            $('#reg_form')[0].reset();
        });
});
//Аякс отправка формы авторизации на сервер и получение результата
$('#auth_form').on('submit',function(e) {
    e.preventDefault();
    var theForm = $(this); //Отправка формы
    $.post("libs/User/goAuth.php", theForm.serialize(), //принимающий файл
        function( data ) {
            if (!data) {
                $(".result").html(data); //Получение результата
                $('#auth_form')[0].reset(); //Очистка формы
            }
            else {
                window.location.reload(); //перезагрузка страницы
                $("#auth_reg").remove();
                $("#auth_reg_close").remove();
                $("#menu_scroll").remove();
                $(".result").html(data); //Вывод результата
            }
        });
});
//Аякс отправка формы добавления нового комментария
$('#new_comment').on('submit',function(e) {
    e.preventDefault();
    var theForm = $(this); //Отправка формы
    $.post("libs/comments/comment.php", theForm.serialize(), //принимающий файл
        function( data ) { //Принимающая функция
           if (!data) { //Если дата отсутствует
               $(".result").html(data); //Вывод результата
               $('#new_comment')[0].reset();//Очистка формы
           }
            else { //Если Data вернула новый комментарий
               $(".result").html(data);    //Вывести его в .result
               document.getElementById('new_comment').style.display = 'none'; //скрыть форму

               //Проверка, parent_id
               if (window.parent_id === '0'){ //Если у нового комментария нет родителя
                   //Проверить, есть ли родительский элемент списка
                   if(!($(".comments").children('ul'))){ //Если нет - добавить
                       $(".comments").append("<ul class='ultra ul-treefree ul-dropfree'></ul>");
                       var adress = $('.comments').children('ul'); //Путь
                       $(".result ul li div:nth-child(3)").css({'background':"yellow"});
                       $(".result ul li").appendTo(adress);
                       $('#new_comment')[0].reset(); //Очистка формы
                       // $(".result ul").remove(); //Удаление пустого поля
                   }
                   else { //Если есть вставить запись в него
                       var adress = $('.comments').children('ul'); //Путь
                       $(".result ul li div:nth-child(3)").css({'background':"yellow"}); //Желтая заливка для нового комментария первого уровня
                       $(".result ul li").appendTo(adress);
                       $('#new_comment')[0].reset(); //Очистка формы
                       // $(".result ul").remove(); //Удаление пустого поля
                   }
               }
               else { //Если у нового комментария есть родитель
                   var adress1 = $('li[data-atr='+window.parent_id+']').children('ul'); //Путь
                   if (adress1.is(':has(ul)')){
                       $(".result ul li div:nth-child(3)").css({'background':"green"}); //Зеленая заливка для дочернего комментария
                       $(".result ul li").appendTo(adress1); //Перемещение
                       // $(this)
                       $('#new_comment')[0].reset(); //Очистка формы
                       $(".result ul").remove(); //Удаление пустого поля
                       delete event(); //Перезапуск события клика
                   }
                   else {
                       // debugger;
                       var parent_li = $('li[data-atr='+window.parent_id+']');
                       parent_li.prepend('<div class="drop"></div>');
                       parent_li.append("<ul class='ultra ul-treefree ul-dropfree'></ul>");
                       var adress2 = parent_li.children('ul'); //Путь
                       $(".result ul li div:nth-child(3)").css({'background':"green"});  //Зеленая заливка для дочернего комментария
                       $(".result ul li").appendTo(adress2);
                       $('#new_comment')[0].reset(); //Очистка формы
                       $(".result ul").remove(); //Удаление пустого поля
                       delete event();
                   }
               }
           }
        });
});
$('#delete_comment').on('submit', function (e) {
    e.preventDefault();
    var theForm = $(this); //Отправка формы
    $.post("libs/comments/delete.php", theForm.serialize(), //принимающий файл
        function ( data ) { //Получение результата
            if ( data === 0){ //Проверка True/False
                // $(".result").html(data); //Вывод результата
                alert('не получилось удалить'); //Сообщение
                document.getElementById('delete_comment').style.display = 'none';
                $('#delete_comment')[0].reset();//Очистка формы
            }
            else {//Проверка True/False
                $(".result").html(data); //Вывод результата
                $('li[data-atr='+window.delete_c+']').remove(); //Удаление пустого поля
                alert('Комментарий удален'); //Сообщение
                document.getElementById('delete_comment').style.display = 'none';
                $('#delete_comment')[0].reset();//Очистка формы
            }
        });
});