function ShowHidden() {
    $(".ul-dropfree").find("li:has(ul)").prepend('<div class="drop"></div>');
}
window.onload = ShowHidden();
$(".comments").on("click", ".ul-dropfree div.drop",(function() {
    if ($(this).nextAll("ul").css('display')=='none') {
        $(this).nextAll("ul").slideDown(400);
        $(this).css({'background-position':"-11px 0"});
    } else {
        $(this).nextAll("ul").slideUp(400);
        $(this).css({'background-position':"0 0"});
    }
}));
$(".ul-dropfree").find("ul").slideUp(1).parents("li").children("div.drop").css({'background-position':"0 0"});
//Функции появления и исчезновения форм регистрации и авторизации
$(".auth_reg").click(function () {
    document.getElementById("auth_reg_close").style.display = 'block';
    document.getElementById('auth_reg_close').style.opacity = '1';
    document.getElementById('menu_scroll').style.marginTop = '0px';
    document.getElementById('menu_scroll').style.position = 'fixed';
});
$(".auth_reg_close").click(function () {
    document.getElementById("auth_reg_close").style.display = 'none';
    document.getElementById("menu_scroll").style.marginTop = '-450px';
    document.getElementById("menu_scroll").style.position = 'absolute';
    document.getElementById("auth_reg_close").style.opacity = '0';
});
//
$(".result").click(function () {
    document.getElementById('message').style.display = 'none';
});
//Функция передачи id родительского комментария дочернему
function event() {
    $(".answer").click(function () {
        document.getElementById('new_comment').style.display = 'block';
        var id_comment = $(this).attr("data-id");
        var to_who = $(this).attr("data-to-who");
        window.parent_id = id_comment;
        window.to_who = to_who;
        if (!document.getElementById('comment_input')) {
            $("#new_comment").append("<input id='comment_input' name='comment' type='hidden' value='" + id_comment + "'>");
            $("#new_comment").append("<input id='to_who' name='where' type='hidden' value='" + to_who + "'>");

        }
        else {
            $("#comment_input").remove();
            $("#to_who").remove();
            $("#new_comment").append("<input id='comment_input' name='comment' type='hidden' value='" + id_comment + "'>");
            $("#new_comment").append("<input id='to_who' name='where' type='hidden' value='" + to_who + "'>");
        }
    });
}
window.onload = event();
//функция скрытия и очистки данных формы отправки комментария
$(".close_new_comment").click(function () {
    document.getElementById('new_comment').style.display = 'none';
    $('#new_comment')[0].reset();
    $("#comment_input").remove();
    $("#to_who").remove();
});
function delete_com() {
    $(".delete").click(function () {
        document.getElementById('delete_comment').style.display = 'block';
        var id_comment_to_del = $(this).attr("data-id-com");
        window.delete_c = id_comment_to_del;
        $("#delete_comment").append("<p><input id='comment_input' name='delete' type='hidden' value='" + id_comment_to_del + "'></p>");
    })
}
window.onload = delete_com();